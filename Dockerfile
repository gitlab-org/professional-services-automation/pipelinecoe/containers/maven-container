FROM registry.gitlab.com/gitlab-org/professional-services-automation/pipelinecoe/containers/container-base:latest

RUN dnf install -y java-11-openjdk wget
RUN wget https://dlcdn.apache.org/maven/maven-3/3.8.6/binaries/apache-maven-3.8.6-bin.tar.gz
RUN tar -xvzf apache-maven-3.8.6-bin.tar.gz -C /opt
RUN ln -s /opt/apache-maven-3.8.6 /opt/maven
ENV M2_HOME="/opt/apache-maven-3.8.6"
RUN full_path=$(readlink -f $(which java))
RUN new_path=$(echo "${full_path/\/bin\/java/""}")
ENV JAVA_HOME="$new_path"
RUN PATH="${PATH}:/opt/apache-maven-3.8.6/bin"
RUN PATH="${PATH}:$new_path/bin"
RUN update-alternatives --install "/usr/bin/mvn" "mvn" "/opt/apache-maven-3.8.6/bin/mvn" 0
RUN update-alternatives --set mvn /opt/apache-maven-3.8.6/bin/mvn

# Leave this so it can't be executed normally.
CMD ["echo", "This is a 'Purpose-Built Container', It is not meant to be ran this way. Please review the documentation on usage."]
